package net.itlabs.pagemodules;

import org.junit.Test;

import static net.itlabs.pagemodules.pages.ToDoMVC.Task.aTask;
import static net.itlabs.pagemodules.pages.ToDoMVC.Type.COMPLETED;
import static net.itlabs.pagemodules.pages.ToDoMVC.*;

/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestOnCompleted {
    
    @Test
    public void testEditOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED));

        startEdit("a", "a edited").pressEnter();
        assertVisibleTasks("a edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED),
                aTask("b"));

        delete("a");
        assertItemsLeft(1);
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testClearCompletedOnCompleted() {
        givenAtCompleted(aTask("a"),
                aTask("c", COMPLETED));

        clearCompleted();
        assertItemsLeft(1);
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testReopenOnCompleted() {
        givenAtCompleted(
                aTask("a", COMPLETED),
                aTask("b"));

        toggle("a");
        assertVisibleTasksAreEmpty();
        assertItemsLeft(2);
    }

    @Test
    public void testReopenAllOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        toggleAll();
        assertVisibleTasks("a", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testCancelEditOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        startEdit("a", "to be canceled").pressEscape();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        startEdit("a", "a edited");
        newTask.click();

        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnCompleted() {
        givenAtCompleted(aTask("a"), aTask("b", COMPLETED));

        startEdit("b", "").pressEnter();
        assertVisibleTasksAreEmpty();
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToAllFromCompleted() {
        givenAtCompleted(aTask("a"),
                aTask("b", COMPLETED));

        openAll();
        assertVisibleTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToActiveFromCompleted() {
        givenAtCompleted(aTask("a"),
                aTask("b", COMPLETED));

        openActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

}
