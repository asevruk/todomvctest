package net.itlabs.fullcovarage;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.Test;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.CollectionCondition.empty;
import static com.codeborne.selenide.CollectionCondition.exactTexts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.url;
import static net.itlabs.fullcovarage.ToDoMVCTest.Task.aTask;
import static net.itlabs.fullcovarage.ToDoMVCTest.Type.ACTIVE;
import static net.itlabs.fullcovarage.ToDoMVCTest.Type.COMPLETED;


public class ToDoMVCTest extends ScreenShotsForEachTest {

    @Test
    public void testE2ESmokeTest() {
        givenAtAll();

        create("a", "b");
        assertVisibleTasks("a", "b");

        //complete
        toggle("a");
        assertVisibleTasks("a", "b");

        openActive();

        startEdit("b", "b edited").pressEnter();
        assertVisibleTasks("b edited");

        openCompleted();

        //reopen
        toggle("a");
        assertVisibleTasksAreEmpty();

        openAll();
        assertVisibleTasks("a", "b edited");

        delete("a");
        assertVisibleTasks("b edited");

        toggleAll();
        assertVisibleTasks("b edited");

        clearCompleted();
        assertVisibleTasksAreEmpty();
    }


    //*********************
    // Tests at All Filter
    //*********************
    @Test
    public void testCreateOnAll() {
        givenAtAll();

        create("a");

        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditOnAll() {
        givenAtAll("a", "b");

        startEdit("a", "a edited").pressEnter();
        assertVisibleTasks("a edited", "b");
        assertItemsLeft(2);
    }

    @Test
    public void testDeleteOnAll() {
        givenAtAll("a", "b");

        delete("a");
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnAll() {
        givenAtAll("a", "b");

        toggle("a");

        assertVisibleTasks("a", "b");
        assertItemsLeft(1);

    }

    @Test
    public void testCompleteAllOnAll() {
        givenAtAll(
                aTask("a"),
                aTask("b"),
                aTask("c", COMPLETED));

        toggleAll();
        assertVisibleTasks("a", "b", "c");
        assertItemsLeft(0);

    }

    @Test
    public void testClearCompletedOnAll() {
        givenAtAll(aTask("a", COMPLETED),
                aTask("c"));

        clearCompleted();
        assertVisibleTasks("c");
        assertItemsLeft(1);
    }

    @Test
    public void testReopenOnAll() {
        givenAtAll(
                aTask("a", COMPLETED),
                aTask("b"));

        toggle("a");
        assertVisibleTasks("a", "b");
        assertItemsLeft(2);
        clearCompltetedButton.should(disappear);
    }

    @Test
    public void testReopenAllOnAll() {
        givenAtAll(
                aTask("a", COMPLETED),
                aTask("c", COMPLETED));

        toggleAll();

        assertVisibleTasks("a", "c");
        assertItemsLeft(2);
        clearCompltetedButton.should(disappear);
    }

    @Test
    public void testCancelEditOnAll() {
        givenAtAll(aTask("a"));

        startEdit("a", "to be canceled").pressEscape();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnAll() {
        givenAtAll(aTask("a"));

        startEdit("a", "a edited");
        newTask.click();

        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnAll() {
        givenAtAll(aTask("a"));

        startEdit("a", "").pressEnter();
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testFilterToActiveFromAll() {
        givenAtAll(aTask("a"),
                aTask("b", COMPLETED));

        openActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToCompletedFromAll() {
        givenAtAll(aTask("a"),
                aTask("b", COMPLETED));

        openCompleted();
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    //*********************
    // Tests at Active Filter
    //*********************


    @Test
    public void testCreateOnActive() {
        givenAtActive();

        create("a");

        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "a edited").pressEnter();
        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActive() {
        givenAtActive("a", "b");

        delete("a");
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnActive() {
        givenAtActive("a");

        toggle("a");
        assertVisibleTasksAreEmpty();

    }

    @Test
    public void testCompleteAllOnActive() {
        givenAtActive(
                aTask("a"),
                aTask("b"),
                aTask("c", COMPLETED));

        toggleAll();
        assertVisibleTasksAreEmpty();

    }

    @Test
    public void testClearCompletedAtActive() {
        givenAtActive(aTask("a", COMPLETED),
                aTask("c", ACTIVE));

        clearCompleted();
        assertVisibleTasks("c");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "to be canceled").pressEscape();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "a edited");
        newTask.click();

        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "").pressEnter();
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testFilterToAllFromActive() {
        givenAtActive(aTask("a"),
                aTask("b", COMPLETED));

        openAll();
        assertVisibleTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToCompletedFromActive() {
        givenAtActive(aTask("a"),
                aTask("b", COMPLETED));

        openCompleted();
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }


    //*********************
    // Tests at Completed  Filter
    //*********************


    @Test
    public void testEditOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED));

        startEdit("a", "a edited").pressEnter();
        assertVisibleTasks("a edited");
        assertItemsLeft(0);
    }

    @Test
    public void testDeleteOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED),
                aTask("b"));

        delete("a");
        assertItemsLeft(1);
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testClearCompletedOnCompleted() {
        givenAtCompleted(aTask("a"),
                aTask("c", COMPLETED));

        clearCompleted();
        assertItemsLeft(1);
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testReopenOnCompleted() {
        givenAtCompleted(
                aTask("a", COMPLETED),
                aTask("b"));

        toggle("a");
        assertVisibleTasksAreEmpty();
        assertItemsLeft(2);
    }

    @Test
    public void testReopenAllOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        toggleAll();
        assertVisibleTasks("a", "b");
        assertItemsLeft(0);
    }

    @Test
    public void testCancelEditOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        startEdit("a", "to be canceled").pressEscape();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnCompleted() {
        givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        startEdit("a", "a edited");
        newTask.click();

        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnCompleted() {
        givenAtCompleted(aTask("a"), aTask("b", COMPLETED));

        startEdit("b", "").pressEnter();
        assertVisibleTasksAreEmpty();
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToAllFromCompleted() {
        givenAtCompleted(aTask("a"),
                aTask("b", COMPLETED));

        openAll();
        assertVisibleTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToActiveFromCompleted() {
        givenAtCompleted(aTask("a"),
                aTask("b", COMPLETED));

        openActive();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }


    ElementsCollection tasks = $$("#todo-list li");
    SelenideElement newTask = $("#new-todo");
    SelenideElement clearCompltetedButton = $("#clear-completed");


    @Step
    public void assertVisibleTasksAreEmpty() {
        tasks.filter(visible).shouldBe(empty);
    }

    @Step
    public void assertVisibleTasks(String... tasksTexts) {
        tasks.filter(visible).shouldHave(exactTexts(tasksTexts));
    }

    @Step
    public void openActive() {
        $(By.linkText("Active")).click();
    }

    @Step
    public void openCompleted() {
        $(By.linkText("Completed")).click();
    }

    @Step
    public void openAll() {
        $(By.linkText("All")).click();
    }

    @Step
    public void create(String... tasksTexts) {
        for (String text : tasksTexts) {
            newTask.setValue(text).pressEnter();
        }
    }

    @Step
    public SelenideElement startEdit(String oldTaskText, String newTaskText) {
        tasks.find(exactText(oldTaskText)).doubleClick();
        return tasks.find(cssClass("editing")).find(".edit").setValue(newTaskText);
    }

    @Step
    public void toggle(String taskText) {
        tasks.find(exactText(taskText)).find(".toggle").click();
    }

    @Step
    public void assertItemsLeft(Integer count) {
        $("#todo-count strong").shouldHave(exactText(String.valueOf(count)));
    }

    @Step
    public void toggleAll() {
        $("#toggle-all").click();
    }

    @Step
    public void delete(String taskText) {
        tasks.find(exactText(taskText)).hover().find(".destroy").click();
    }

    @Step
    public void clearCompleted() {
        $("#clear-completed").click();
    }

    public enum Type {ACTIVE, COMPLETED}

    public enum Filter {

        ALL(""), ACTIVE("#/active"), COMPLETED("#/completed");

        Filter(String subURL) {
            this.subUrl = subURL;
        }

        public String subUrl;
    }

    public static class Task {
        String name;
        Type status;

        public Task(String name, Type status) {
            this.name = name;
            this.status = status;
        }

        public static Task aTask(String name, Type status) {
            return new Task(name, status);
        }

        public static Task aTask(String name) {
            return new Task(name, ACTIVE);
        }
    }

    private void given(Filter filter, Task... tasks) {
        ensureOpenPage(filter);
        String tempResultJS = "localStorage.setItem(\"todos-troopjs\", \"[";

        for (Task task : tasks) {
            tempResultJS += "{\\\"completed\\\":" + (task.status == ACTIVE ? false : true) + ", \\\"title\\\":\\\"" + task.name + "\\\"},";

        }
        if (tasks.length > 0) {
            tempResultJS = tempResultJS.substring(0, tempResultJS.length() - 1) + "]\")";
        } else {
            tempResultJS += "]\")";
        }

        executeJavaScript(tempResultJS);

        executeJavaScript("location.reload()");
    }

    private Task[] convertToTasks(String... texts) {

        Task[] arrayTask = new Task[texts.length];

        for (int i = 0; i < texts.length; i++) {
            arrayTask[i] = aTask(texts[i], ACTIVE);
        }
        return arrayTask;
    }

    public void givenAtAll() {
        given(Filter.ALL);
    }

    public void givenAtAll(Task... tasks) {
        given(Filter.ALL, tasks);
    }

    public void givenAtAll(String... taskTexts) {
        given(Filter.ALL, convertToTasks(taskTexts));
    }

    public void givenAtActive() {
        given(Filter.ACTIVE);
    }

    public void givenAtActive(Task... tasks) {
        given(Filter.ACTIVE, tasks);
    }

    public void givenAtActive(String... taskTexts) {
        given(Filter.ACTIVE, convertToTasks(taskTexts));
    }

    public void givenAtCompleted(Task... tasks) {
        given(Filter.COMPLETED, tasks);
    }

    public void givenCompleted(String... taskTexts) {
        given(Filter.COMPLETED, convertToTasks(taskTexts));
    }

    public void ensureOpenPage(Filter filter) {
        String toDOMVCUrl = "https://todomvc4tasj.herokuapp.com/";

        if (!(url().equals(toDOMVCUrl + filter.subUrl)))
            open(toDOMVCUrl + filter.subUrl);

    }


}

