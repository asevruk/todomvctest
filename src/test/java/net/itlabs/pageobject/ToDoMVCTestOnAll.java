package net.itlabs.pageobject;

import net.itlabs.pageobject.pages.ToDoMVCPage;
import org.junit.Test;

import static com.codeborne.selenide.Condition.disappear;
import static net.itlabs.pageobject.pages.ToDoMVCPage.Task.aTask;
import static net.itlabs.pageobject.pages.ToDoMVCPage.Type.COMPLETED;



/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestOnAll {
    ToDoMVCPage page= new ToDoMVCPage();
    @Test
    public void testCreateOnAll() {
        page.givenAtAll();

        page.create("a");

        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditOnAll() {
        page.givenAtAll("a", "b");

        page.startEdit("a", "a edited").pressEnter();
        page.assertVisibleTasks("a edited", "b");
        page.assertItemsLeft(2);
    }

    @Test
    public void testDeleteOnAll() {
        page.givenAtAll("a", "b");

        page.delete("a");
        page.assertVisibleTasks("b");
        page.assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnAll() {
        page.givenAtAll("a", "b");

        page.toggle("a");

        page.assertVisibleTasks("a", "b");
        page.assertItemsLeft(1);

    }

    @Test
    public void testCompleteAllOnAll() {
        page.givenAtAll(aTask("a"),
                aTask("b"),
                aTask("c", COMPLETED));

        page.toggleAll();
        page.assertVisibleTasks("a", "b", "c");
        page.assertItemsLeft(0);

    }

    @Test
    public void testClearCompletedOnAll() {
        page.givenAtAll(aTask("a", COMPLETED),
                aTask("c"));

        page.clearCompleted();
        page.assertVisibleTasks("c");
        page.assertItemsLeft(1);
    }

    @Test
    public void testReopenOnAll() {
        page.givenAtAll(
                aTask("a", COMPLETED),
                aTask("b"));

        page.toggle("a");
        page.assertVisibleTasks("a", "b");
        page.assertItemsLeft(2);
        page.clearCompltetedButton.should(disappear);
    }

    @Test
    public void testReopenAllOnAll() {
        page.givenAtAll(
                aTask("a", COMPLETED),
                aTask("c", COMPLETED));

        page.toggleAll();

        page.assertVisibleTasks("a", "c");
        page.assertItemsLeft(2);
        page.clearCompltetedButton.should(disappear);
    }

    @Test
    public void testCancelEditOnAll() {
        page.givenAtAll(aTask("a"));

        page.startEdit("a", "to be canceled").pressEscape();
        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnAll() {
        page.givenAtAll(aTask("a"));

        page.startEdit("a", "a edited");
        page.newTask.click();

        page.assertVisibleTasks("a edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnAll() {
        page.givenAtAll(aTask("a"));

        page.startEdit("a", "").pressEnter();
        page.assertVisibleTasksAreEmpty();
    }

    @Test
    public void testFilterToActiveFromAll() {
        page.givenAtAll(aTask("a"),
                aTask("b", COMPLETED));

        page.openActive();
        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testFilterToCompletedFromAll() {
        page.givenAtAll(aTask("a"),
                aTask("b", COMPLETED));

        page.openCompleted();
        page.assertVisibleTasks("b");
        page.assertItemsLeft(1);
    }

}
