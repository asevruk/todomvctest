package net.itlabs.pagemodules;

import org.junit.Test;

import static net.itlabs.pagemodules.pages.ToDoMVC.*;

/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestE2E {
    

    @Test
    public void testE2ESmokeTest() {
        givenAtAll();

        create("a", "b");
        assertVisibleTasks("a", "b");

        //complete
        toggle("a");
        assertVisibleTasks("a", "b");

        openActive();

        startEdit("b", "b edited").pressEnter();
        assertVisibleTasks("b edited");

        openCompleted();

        //reopen
        toggle("a");
        assertVisibleTasksAreEmpty();

        openAll();
        assertVisibleTasks("a", "b edited");

        delete("a");
        assertVisibleTasks("b edited");

        toggleAll();
        assertVisibleTasks("b edited");

        clearCompleted();
        assertVisibleTasksAreEmpty();
    }
}
