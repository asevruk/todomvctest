package net.itlabs.pagemodules;

import org.junit.Test;

import static net.itlabs.pagemodules.pages.ToDoMVC.Task.aTask;
import static net.itlabs.pagemodules.pages.ToDoMVC.Type.ACTIVE;
import static net.itlabs.pagemodules.pages.ToDoMVC.Type.COMPLETED;
import static net.itlabs.pagemodules.pages.ToDoMVC.*;


/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestOnActive {
    
    @Test
    public void testCreateOnActive() {
        givenAtActive();

        create("a");

        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAtActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "a edited").pressEnter();
        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActive() {
        givenAtActive("a", "b");

        delete("a");
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnActive() {
        givenAtActive("a");

        toggle("a");
        assertVisibleTasksAreEmpty();

    }

    @Test
    public void testCompleteAllOnActive() {
        givenAtActive(
                aTask("a"),
                aTask("b"),
                aTask("c", COMPLETED));

        toggleAll();
        assertVisibleTasksAreEmpty();

    }

    @Test
    public void testClearCompletedAtActive() {
        givenAtActive(aTask("a", COMPLETED),
                aTask("c", ACTIVE));

        clearCompleted();
        assertVisibleTasks("c");
        assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "to be canceled").pressEscape();
        assertVisibleTasks("a");
        assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "a edited");
        newTask.click();

        assertVisibleTasks("a edited");
        assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnActive() {
        givenAtActive(aTask("a"));

        startEdit("a", "").pressEnter();
        assertVisibleTasksAreEmpty();
    }

    @Test
    public void testFilterToAllFromActive() {
        givenAtActive(aTask("a"),
                aTask("b", COMPLETED));

        openAll();
        assertVisibleTasks("a", "b");
        assertItemsLeft(1);
    }

    @Test
    public void testFilterToCompletedFromActive() {
        givenAtActive(aTask("a"),
                aTask("b", COMPLETED));

        openCompleted();
        assertVisibleTasks("b");
        assertItemsLeft(1);
    }

}
