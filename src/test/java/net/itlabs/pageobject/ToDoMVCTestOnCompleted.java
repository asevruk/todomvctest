package net.itlabs.pageobject;

import net.itlabs.pageobject.pages.ToDoMVCPage;
import org.junit.Test;

import static net.itlabs.pageobject.pages.ToDoMVCPage.Task.aTask;
import static net.itlabs.pageobject.pages.ToDoMVCPage.Type.*;

/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestOnCompleted {
    ToDoMVCPage page = new ToDoMVCPage();
    @Test
    public void testEditOnCompleted() {
        page.givenAtCompleted(aTask("a", COMPLETED));

        page.startEdit("a", "a edited").pressEnter();
        page.assertVisibleTasks("a edited");
        page.assertItemsLeft(0);
    }

    @Test
    public void testDeleteOnCompleted() {
        page.givenAtCompleted(aTask("a", COMPLETED),
                aTask("b"));

        page.delete("a");
        page.assertItemsLeft(1);
        page.assertVisibleTasksAreEmpty();
    }

    @Test
    public void testClearCompletedOnCompleted() {
        page.givenAtCompleted(aTask("a"),
                aTask("c", COMPLETED));

        page.clearCompleted();
        page.assertItemsLeft(1);
        page.assertVisibleTasksAreEmpty();
    }

    @Test
    public void testReopenOnCompleted() {
        page.givenAtCompleted(
                aTask("a", COMPLETED),
                aTask("b"));

        page.toggle("a");
        page.assertVisibleTasksAreEmpty();
        page.assertItemsLeft(2);
    }

    @Test
    public void testReopenAllOnCompleted() {
        page.givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        page.toggleAll();
        page.assertVisibleTasks("a", "b");
        page.assertItemsLeft(0);
    }

    @Test
    public void testCancelEditOnCompleted() {
        page.givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        page.startEdit("a", "to be canceled").pressEscape();
        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnCompleted() {
        page.givenAtCompleted(aTask("a", COMPLETED), aTask("b"));

        page.startEdit("a", "a edited");
        page.newTask.click();

        page.assertVisibleTasks("a edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnCompleted() {
        page.givenAtCompleted(aTask("a"), aTask("b", COMPLETED));

        page.startEdit("b", "").pressEnter();
        page.assertVisibleTasksAreEmpty();
        page.assertItemsLeft(1);
    }

    @Test
    public void testFilterToAllFromCompleted() {
        page.givenAtCompleted(aTask("a"),
                aTask("b", COMPLETED));

        page.openAll();
        page.assertVisibleTasks("a", "b");
        page.assertItemsLeft(1);
    }

    @Test
    public void testFilterToActiveFromCompleted() {
        page.givenAtCompleted(aTask("a"),
                aTask("b", COMPLETED));

        page.openActive();
        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

}
