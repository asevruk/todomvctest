package net.itlabs.pageobject;

import net.itlabs.pageobject.pages.ToDoMVCPage;
import org.junit.Test;

import static net.itlabs.pageobject.pages.ToDoMVCPage.Task.aTask;
import static net.itlabs.pageobject.pages.ToDoMVCPage.Type.COMPLETED;
import static net.itlabs.pageobject.pages.ToDoMVCPage.Type.ACTIVE;

/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestOnActive {
    ToDoMVCPage page =new ToDoMVCPage();

    @Test
    public void testCreateOnActive() {
        page.givenAtActive();

        page.create("a");

        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditAtActive() {
        page.givenAtActive(aTask("a"));

        page.startEdit("a", "a edited").pressEnter();
        page.assertVisibleTasks("a edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteAtActive() {
        page.givenAtActive("a", "b");

        page.delete("a");
        page.assertVisibleTasks("b");
        page.assertItemsLeft(1);
    }

    @Test
    public void testCompleteOnActive() {
        page.givenAtActive("a");

        page.toggle("a");
        page.assertVisibleTasksAreEmpty();

    }

    @Test
    public void testCompleteAllOnActive() {
        page.givenAtActive(
                aTask("a"),
                aTask("b"),
                aTask("c", COMPLETED));

        page.toggleAll();
        page.assertVisibleTasksAreEmpty();

    }

    @Test
    public void testClearCompletedAtActive() {
        page.givenAtActive(aTask("a", COMPLETED),
                aTask("c", ACTIVE));

        page.clearCompleted();
        page.assertVisibleTasks("c");
        page.assertItemsLeft(1);
    }

    @Test
    public void testCancelEditOnActive() {
        page.givenAtActive(aTask("a"));

        page.startEdit("a", "to be canceled").pressEscape();
        page.assertVisibleTasks("a");
        page.assertItemsLeft(1);
    }

    @Test
    public void testEditAndClickOutsideOnActive() {
        page.givenAtActive(aTask("a"));

        page.startEdit("a", "a edited");
        page.newTask.click();

        page.assertVisibleTasks("a edited");
        page.assertItemsLeft(1);
    }

    @Test
    public void testDeleteByEditingOnActive() {
        page.givenAtActive(aTask("a"));

        page.startEdit("a", "").pressEnter();
        page.assertVisibleTasksAreEmpty();
    }

    @Test
    public void testFilterToAllFromActive() {
        page.givenAtActive(aTask("a"),
                aTask("b", COMPLETED));

        page.openAll();
        page.assertVisibleTasks("a", "b");
        page.assertItemsLeft(1);
    }

    @Test
    public void testFilterToCompletedFromActive() {
        page.givenAtActive(aTask("a"),
                aTask("b", COMPLETED));

        page.openCompleted();
        page.assertVisibleTasks("b");
        page.assertItemsLeft(1);
    }

}
