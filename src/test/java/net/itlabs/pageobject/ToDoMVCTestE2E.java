package net.itlabs.pageobject;

import net.itlabs.pageobject.pages.ToDoMVCPage;
import org.junit.Test;

/**
 * Created by ������ on 09.02.2016.
 */
public class ToDoMVCTestE2E {
    ToDoMVCPage page = new ToDoMVCPage();

    @Test
    public void testE2ESmokeTest() {
        page.givenAtAll();

        page.create("a", "b");
        page.assertVisibleTasks("a", "b");

        //complete
        page.toggle("a");
        page.assertVisibleTasks("a", "b");

        page.openActive();

        page.startEdit("b", "b edited").pressEnter();
        page.assertVisibleTasks("b edited");

        page.openCompleted();

        //reopen
        page.toggle("a");
        page.assertVisibleTasksAreEmpty();

        page.openAll();
        page.assertVisibleTasks("a", "b edited");

        page.delete("a");
        page.assertVisibleTasks("b edited");

        page.toggleAll();
        page.assertVisibleTasks("b edited");

        page.clearCompleted();
        page.assertVisibleTasksAreEmpty();
    }
}
